﻿using PersonMVC.Models.Entities;
using PersonMVC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PersonMVC.Repositories
{
    public class PersonRepository
    {
        private string connectionString 
        {
            get 
            {
                return @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\User\Source\Repos\kbtu_proj\PersonMVC\App_Data\localStorageDB.mdf;Integrated Security=True";
            }
        }

        
        public ICollection<Country> GetCountries()
        {
            var _out = new List<Country>();

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "CountriesSelectProcedure";

            con.Open();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    _out.Add(new Country() 
                    {
                        ID = reader.GetInt32(0),
                        CountryName = reader.GetString(1)
                    });
                }
            }

            con.Close();

            return _out;
        }

        /*
        public ICollection<Person> GetPersons(int _currentID, int actionType, int limit)
        {
            throw new NotImplementedException();
        }

        public ICollection<Person> GetPersons()
        {
            throw new NotImplementedException();
        }
        */

        
        public PersonsResponceVM GetPersonResponce()
        {
            // start dates
            var _currentID = 0;
            var actionType = 1;
            var limit = 5;

            var _out = new List<Person>();

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "PersonsSelectProcedure";
            cmd.Parameters.AddWithValue("@actionType", actionType);
            cmd.Parameters.AddWithValue("@currentID", _currentID);
            cmd.Parameters.AddWithValue("@limit", limit);

            con.Open();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    _out.Add(new Person()
                    {
                        ID = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = reader.GetString(3)
                    });
                }
            }

            con.Close();

            PersonsResponceVM res = new PersonsResponceVM()
            {
                Persons = Person.ConvertTo(_out),
                minIndex = _out.Min(x => x.ID),
                maxIndex = _out.Max(x => x.ID)
            };


            return res;
        }

        public PersonsResponceVM GetPersonResponce(int _currentID, int actionType, int limit) 
        {
            var _out = new List<Person>();

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "PersonsSelectProcedure";
            cmd.Parameters.AddWithValue("@actionType", actionType);
            cmd.Parameters.AddWithValue("@currentID", _currentID);
            cmd.Parameters.AddWithValue("@limit", limit);

            con.Open();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    _out.Add(new Person()
                    {
                        ID = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        LastName = reader.GetString(2),
                        Country = reader.GetString(3)
                    });
                }
            }

            //string EmployeeId = outPutParameter.Value.ToString();

            con.Close();

            PersonsResponceVM res = new PersonsResponceVM() 
            {
                Persons = Person.ConvertTo(_out),
                minIndex = _out.Min(x => x.ID),
                maxIndex = _out.Max(x => x.ID)
            };
            

            return res;
        }
        
        public void AddPerson(Person person)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "PersonAddProcedure";
            cmd.Parameters.AddWithValue("@fName", person.FirstName);
            cmd.Parameters.AddWithValue("@lName", person.LastName);
            cmd.Parameters.AddWithValue("@countryId", person.CountryID);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void EditPerson(Person person)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "PersonEditProcedure";
            cmd.Parameters.AddWithValue("@id", person.ID);
            cmd.Parameters.AddWithValue("@fName", person.FirstName);
            cmd.Parameters.AddWithValue("@lName", person.LastName);
            cmd.Parameters.AddWithValue("@countryID", person.CountryID);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void DeletePerson(Person person)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "PersonDeleteProcedure";
            cmd.Parameters.AddWithValue("@id", person.ID);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}