﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonMVC.Models.ViewModels
{
    public class PersonVM
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }
    }
}