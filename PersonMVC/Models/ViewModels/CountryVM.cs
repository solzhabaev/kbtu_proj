﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonMVC.Models.ViewModels
{
    public class CountryVM
    {
        public int ID { get; set; }

        public string CountryName { get; set; }
    }
}