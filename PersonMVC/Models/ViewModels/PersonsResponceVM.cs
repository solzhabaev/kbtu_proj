﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonMVC.Models.ViewModels
{
    public class PersonsResponceVM
    {
        public List<PersonVM> Persons { get; set; }

        public int minIndex { get; set; }

        public int maxIndex { get; set; }

    }
}