﻿using PersonMVC.Models.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonMVC.Models.Entities
{
    public class Country
    {
        public int ID { get; set; }

        public string CountryName { get; set; }

        public static List<CountryVM> ConvertTo(ICollection<Country> countries) 
        {
            var _counts = countries;

            List<CountryVM> res = new List<CountryVM>();

            foreach(var _cntr in _counts)
            {
                res.Add(new CountryVM() 
                {
                    ID = _cntr.ID, 
                    CountryName = _cntr.CountryName
                });
            }

            return res;
        }
    }
}