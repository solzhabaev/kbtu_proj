﻿using PersonMVC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonMVC.Models.Entities
{
    public class Person
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int CountryID { get; set; }

        public string Country { get; set; }

        public static List<PersonVM> ConvertTo(ICollection<Person> persons) 
        {
            var _prsns = persons;

            List<PersonVM> res = new List<PersonVM>();

            foreach(var _per in _prsns)
            {
                res.Add(new PersonVM() 
                {
                    ID = _per.ID,
                    FirstName = _per.FirstName,
                    LastName = _per.LastName,
                    Country = _per.Country
                });
            }

            return res;
        }

    }
}