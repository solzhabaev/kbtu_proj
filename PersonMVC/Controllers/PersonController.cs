﻿using PersonMVC.Models.Entities;
using PersonMVC.Models.ViewModels;
using PersonMVC.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PersonMVC.Controllers
{
    public class PersonController : Controller
    {
        private PersonRepository personRepository;

        //
        // GET: /Person/

        public ActionResult Index()
        {
            personRepository = new PersonRepository();

            var persons = personRepository.GetPersonResponce();

            return Json(persons);
        }

        public ActionResult CountryGet() 
        {
            personRepository = new PersonRepository();

            var counties = Country.ConvertTo(personRepository.GetCountries());

            return Json(counties);
        }

         // GET: Person/PersonGet/0
        public ActionResult PersonGet(int currentId, int actionType, int limit) 
        {
            personRepository = new PersonRepository();

            var persons = personRepository.GetPersonResponce(currentId, actionType, limit);

            return Json(persons);
        }

        // Person/PersonAdd
        public void PersonAdd(string firstName, string lastName, int countryID)
        {
            personRepository = new PersonRepository();

            personRepository.AddPerson(new Person() { FirstName = firstName, LastName = lastName, CountryID = countryID });
        }

        // Person/PersonEdit
        public void PersonEdit(int personID, string firstName, string lastName, int countryID)
        {
            personRepository = new PersonRepository();
            
            personRepository.EditPerson(new Person() { ID = personID, FirstName = firstName, LastName = lastName, CountryID = countryID });
        }

        // Person/PersonDelete
        public void PersonDelete(int personID)
        {
            personRepository = new PersonRepository();
            
            personRepository.DeletePerson(new Person() { ID = personID });
        }

    }
}
