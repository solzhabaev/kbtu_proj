﻿var appMainModule = angular.module('appMain', []);

appMainModule.controller("homePageViewModel", function ($scope, $http, $location) {
    // properties on form
    $scope.Persons = [];
    $scope.PersonsMinID = {};
    $scope.PersonsMaxID = {};

    $scope.PersonSelected = {};

    $scope.PersonOnForm = {};

    // 1 - add
    // 2 - edit
    $scope.OperationType = {};

    $scope.Countries = [];

    $scope.CountrySelected = {};

    $scope.RowsCountLoad = 5;

    // here init ---> Page.load()
    $scope.init = function () {
        $http({ method: 'POST', url: '/Person/' })
        .success(function (data) {
            console.log(data);
            $scope.Persons = data.Persons;
            $scope.PersonsMinID = data.minIndex;
            $scope.PersonsMaxID = data.maxIndex;
        });
        $http({ method: 'POST', url: '/Person/CountryGet' })
        .success(function (data) {
            console.log(data);
            $scope.Countries = data;
        });
    };

    $scope.LoadDatesRefresh = function (){
        $http({
            method: 'POST', url: '/Person/PersonGet/', data: {
                currentId: 0,
                actionType: 1,
                limit: $scope.RowsCountLoad
            }
        })
        .success(function (data) {
            console.log(data);
            $scope.Persons = data.Persons;
            $scope.PersonsMinID = data.minIndex;
            $scope.PersonsMaxID = data.maxIndex;
        });
    }

    $scope.selectPerson = function (person) {
        $scope.PersonSelected = person;
        console.log($scope.PersonSelected);
    }



    $scope.loadPersonsNext = function () {
        $http({ method: 'POST', url: '/Person/PersonGet/', data: { 
            currentId: $scope.PersonsMaxID, 
            actionType: 1,
            limit: $scope.RowsCountLoad
        } })
        .success(function (data) {
            console.log(data);
            $scope.Persons = data.Persons;
            $scope.PersonsMinID = data.minIndex;
            $scope.PersonsMaxID = data.maxIndex;
        });
    }

    $scope.loadPersonsBack = function () {
        $http({
            method: 'POST', url: '/Person/PersonGet/', data: {
            currentId: $scope.PersonsMinID,
            actionType: 0,
            limit: $scope.RowsCountLoad
        } })
        .success(function (data) {
            console.log(data);
            $scope.Persons = data.Persons;
            $scope.PersonsMinID = data.minIndex;
            $scope.PersonsMaxID = data.maxIndex;
        });
    }

    $scope.AddPersonClick = function () {
        $scope.PersonOnForm = {};
        $scope.OperationType = 1;
    }

    $scope.EditPersonClick = function () {
        $scope.PersonOnForm = $scope.PersonSelected;
        $scope.OperationType = 2;
    }

    $scope.DeletePersonClick = function () {
        $scope.PersonOnForm = $scope.PersonSelected;
    }

    $scope.SubmitChanges = function () {
        if ($scope.OperationType == 1) {
                $http({
                    method: 'POST', url: '/Person/PersonAdd/', data: {
                        firstName: $scope.PersonOnForm.FirstName,
                        lastName: $scope.PersonOnForm.LastName,
                        countryID: $scope.CountrySelected.ID
                    }
                })
            .success(function () {
            console.log("added");
            }); } else if ($scope.OperationType == 2) {
                $http({
                    method: 'POST', url: '/Person/PersonEdit/', data: {
                        personID : $scope.PersonOnForm.ID,
                        firstName: $scope.PersonOnForm.FirstName,
                        lastName: $scope.PersonOnForm.LastName,
                        countryID: $scope.CountrySelected.ID
                    }
                })
                .success(function () {
                    console.log("Edited");
                });
            }
        $scope.CancelAll();
    }

    $scope.SubmitChangesDelete = function () {
        $http({
            method: 'POST', url: '/Person/PersonDelete/', data: {
                personID: $scope.PersonOnForm.ID
            }
        })
                .success(function () {
                    console.log("deleted");
                });
        $scope.CancelAll();
    }

    $scope.CancelAll = function () {
        $scope.PersonOnForm = {};
        $('#myModal1').modal('hide');
        $('#myModal2').modal('hide');
    }


});